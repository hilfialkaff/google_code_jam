#include <iostream>
#include <map>
#include <cstring>
#include <fstream>
#include <string>
#include <stdlib.h>

using namespace std;

static const char test_in[] = {"ejp mysljylc kd kxveddknmc re jsicpdrysi rbcpc ypc rtcsra dkh wyfrepkym veddknkmkrkcd de kr kd eoya kw aej tysr re ujdr lkgc jv"};

static const char test_out[] = {"our language is impossible to understand there are twenty six factorial possibilities so it is okay if you want to just give up"};

int main()
{
    map<char, char> chars;
    int num_test;
    ofstream out;
    string str;
    string::iterator it;

    chars.insert(pair<char, char>('q', 'z'));
    chars.insert(pair<char, char>('z', 'q'));
    for (int i = 0; i < strlen(test_in); i++) {
        if (chars.find(test_in[i]) == chars.end()) {
            chars.insert(pair<char, char>(test_in[i], test_out[i]));
        }
    }

    out.open("a.out");
    getline(cin, str);
    num_test = atoi(str.c_str());

    for (int i = 0; i < num_test; i++) {
        getline(cin, str);
        out << "Case #" << i+1 << ": ";

        for (it = str.begin(); it != str.end(); it++) {
            if ((*it) == ' ') {
                out << ' ';
            }
            out << chars.find(char(*it))->second;
        }
        out << endl;
    }
}
