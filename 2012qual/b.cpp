#include <iostream>
#include <fstream>

using namespace std;

int main()
{
    int t, n, s, p, score, total;
    ofstream out;
    out.open("b.out");

    cin >> t;

    for (int i = 0; i < t; i++) {
        total = 0;
        cin >> n >> s >> p;

        for (int j = 0; j < n; j++) {
            cin >> score;

            if ((score + 2)/3 >= p) {
                total++;
            } else if (score >= 29 || score <= 1) {
                continue; 
            } else if ((score + 4)/3 >= p && s > 0) {
                total++;
                s--;
            }
        }

        out << "Case #" << i + 1 << ": " << total << endl;
    }

    out.close();
    return 0;
}
