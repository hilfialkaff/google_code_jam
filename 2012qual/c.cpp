#include <iostream>
#include <sstream>
#include <string>
#include <cstring>
#include <fstream>

using namespace std;

string convertInt(int num)
{
    stringstream ss;
    ss << num;
    return ss.str();
}

int main()
{
    int n;
    int count;
    ofstream out;

    out.open("c.out");
    cin >> n;

    for (int i = 0; i < n; i++) {
        int num1, num2;

        count = 0;

        cin >> num1 >> num2;

        for (int j = num1; j <= num2; j++) {
            for (int k = j + 1; k <= num2; k++) {
                string s1, s2;

                s1 = convertInt(j);
                s2 = convertInt(k);

                if (s1.length() != s2.length()) {
                    break;
                }

                for (int l = 1; l < s1.length(); l++) {
                    if (s2 == (s1.substr(l) + s1.substr(0, l))) {
                        count++;
                        break;
                    }
                }
            }
        }

        out << "Case #" << i + 1 << ": " << count << endl; 
    }

    out.close();
    return 0;
}
