#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <cstdlib>

using namespace std;

int main()
{
    string str;
    int num_test, total, typed, i, j;
    double tmp, prob, best_stroke, num_stroke = 0;
    vector<double> p;
    vector<double>::iterator it;

    cin >> num_test;

    for (i = 0; i < num_test; i++) {
        cin >> typed >> total;
        for (j = 0; j < typed; j++) {
            cin >> tmp;
            p.push_back(tmp);
        }

        best_stroke = total + 2;
        prob = 1;

        for (j = 0, it = p.begin(); j <= typed; it++, j++) {
            prob *= *it;
            num_stroke  = (prob * ((typed - j) + (total - j) + 1)) + ((1 - prob) * ((typed - j) + (total - j) + + 1 + total + 1));
            best_stroke = (num_stroke < best_stroke) ? num_stroke : best_stroke;
        }

        cout << "Case #" << i + 1 << ": " << fixed << setprecision(6) << best_stroke << endl;
        p.clear();
    }

    return 0;
}
