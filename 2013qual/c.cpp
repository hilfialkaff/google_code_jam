#include <iostream>
#include <vector>
#include <map>
#include <cstring>
#include <string>
#include <fstream>
#include <sstream>
#include <string>
#include <stdlib.h>
#include <cmath>

using namespace std;

vector<long long> palindromes;

string convert_int(long long num)
{
    stringstream ss;
    ss << num;
    return ss.str();
}

bool is_palindrome(long long num)
{
    string str = convert_int(num);
    int len = str.length();
    bool ret = true;

    for (int i = 0; i < len; i++) {
        if (str[i] != str[len - 1 - i]) {
            ret = false;
            break;
        }
    }

    return ret;
}

int main()
{
    int T;
    long long A, B;
    long long start;
    int count;

    cin >> T;
    for (int i = 1; i <= T; i++) {
        count = 0;
        cin >> A >> B;

        start = sqrt(A);
        if ((start * start) <  A) {
            start++;
        }

        while((start * start) <= B) {
            if (is_palindrome (start) && is_palindrome(start * start)) {
                count++;
            }

            start++;
        }

        cout << "Case #" << i << ": " << count << endl;
    }
}
