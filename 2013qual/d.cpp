#include <iostream>
#include <algorithm>
#include <vector>
#include <map>
#include <cstring>
#include <fstream>
#include <string>
#include <stdlib.h>

using namespace std;

#define FOR(i, init, end) for(int i=(init); i < (end); i++)

int T, K, N;

vector<int> solve(vector<int> keys, vector< pair<int, bool> > key_for_chests, vector<int> chest_keys[], vector<int> tried_chests) {
    int k;
    vector<int>::iterator it;
    vector<int> result;

    if (tried_chests.size() == key_for_chests.size()) {
        return tried_chests;
    }

    cout << "tried:";
    for (int i = 0; i < tried_chests.size(); i++) {
        cout << " " << tried_chests[i] + 1;
    }
    cout << endl;

    for (int i = 0; i < key_for_chests.size(); i++) {
        vector< pair<int, bool> > new_status = key_for_chests;
        vector<int> new_tried = tried_chests;
        vector<int> new_keys = keys;

        if (new_status[i].second) {
            cout << "visited: " << i << endl;
            continue;
        }

        k = new_status[i].first;
        it = find(new_keys.begin(), new_keys.end(), k);

        if (it == new_keys.end()) {
            cout << "no key: " << k << endl;
            continue;
        }

        new_status[i].second = true;
        new_keys.erase(it);
        for (int j = 0; j < chest_keys[i].size(); j++) {
            new_keys.push_back(chest_keys[i][j]);
        }

        cout << "cur new_keys:";
        FOR(k, 0, new_keys.size()) {
            cout << " " << new_keys[k];
        }
        cout << endl;

        new_tried.push_back(i);
        result = solve(new_keys, new_status, chest_keys, new_tried);

        if (result.size() == key_for_chests.size()) {
            break;
        }
    }

    return result;
}

int main()
{
    int tmp1, tmp2;
    vector<int> keys;
    vector< pair<int, bool> > key_for_chests;
    vector<int> tried_chests;
    vector<int> result;

    cin >> T;
    for (int i = 1; i <= T; i++) {
        cin >> K >> N;
        vector<int> chest_keys[N];

        for (int j = 0; j < K; j++) {
            cin >> tmp1;
            keys.push_back(tmp1);
        }

        for (int j = 0; j < N; j++) {
            cin >> tmp1;
            key_for_chests.push_back(pair<int, bool>(tmp1, false));

            cin >> tmp1;
            for (int k = 0; k < tmp1; k++) {
                cin >> tmp2;
                chest_keys[j].push_back(tmp2);
            }
        }

        result = solve(keys, key_for_chests, chest_keys, tried_chests);
        
        cout << "Case #" << i << ":";
        if (result.size() != N) {
            cout << " IMPOSSIBLE";
        } else {
            for (int j = 0; j < result.size(); j++) {
                cout << " " << result[j] + 1;
            }
        }

        cout << endl;

        keys.clear();
        key_for_chests.clear();
        tried_chests.clear();
    }
}
